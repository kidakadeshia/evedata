-- 356866 (Higher Price)
-- 355811 (Lower Price)
-- 353742 (Dropsuit)
-- 355413 (Vehicle)
-- 356799 (Dropsuit)
-- 356862 (Vehicle)
-- 355416 (Duplicate 1)
-- 356865 (Duplicate 2)
-- 352069 (Vehicle)
-- 8745   (Ship)
-- 352076 (Vehicle)
-- 1539   (Ship)

/*
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 356866), ' (Higher Price)' COLLATE Latin1_General_CI_AI) WHERE typeID = 356866;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 355811), ' (Lower Price)' COLLATE Latin1_General_CI_AI) WHERE typeID = 355811;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 353742), ' (Dropsuit)' COLLATE Latin1_General_CI_AI) WHERE typeID = 353742;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 355413), ' (Vehicle)' COLLATE Latin1_General_CI_AI) WHERE typeID = 355413;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 356799), ' (Dropsuit)' COLLATE Latin1_General_CI_AI) WHERE typeID = 356799;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 356862), ' (Vehicle)' COLLATE Latin1_General_CI_AI) WHERE typeID = 356862;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 355416), ' (Duplicate 1)' COLLATE Latin1_General_CI_AI) WHERE typeID = 355416;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 356865), ' (Duplicate 2)' COLLATE Latin1_General_CI_AI) WHERE typeID = 356865;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 352069), ' (Vehicle)' COLLATE Latin1_General_CI_AI) WHERE typeID = 352069;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 8745), ' (Ship)' COLLATE Latin1_General_CI_AI) WHERE typeID = 8745;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 352076), ' (Vehicle)' COLLATE Latin1_General_CI_AI) WHERE typeID = 352076;
UPDATE invTypes SET typeName = CONCAT((SELECT tt.typeName FROM invTypes AS tt WHERE typeID = 1539), ' (Ship)' COLLATE Latin1_General_CI_AI) WHERE typeID = 1539;
*/

-- 1000193 NULL faction

/*
UPDATE crpNPCCorporations SET factionID = NULL WHERE corporationID = 1000193;
*/
