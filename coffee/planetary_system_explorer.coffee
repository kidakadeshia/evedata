
class PlanetarySystemExplorer
  constructor: (@containerId, @width = 700, @height = 500, initialSystem = null) ->
    # Make this stuff static
    @_planetColors =
      'Planet (Temperate)': "#00FF00"
      'Planet (Ice)': "#819FF7"
      'Planet (Gas)': "#393E06"
      'Moon': "#AAAAAA"
      'Asteroid Belt': "#F4FF08"
      'Planet (Oceanic)': "#0000FF"
      'Planet (Lava)': "#FF0000"
      'Planet (Barren)': "#886A08"
      'Planet (Storm)': "#FF6302"
      'Planet (Plasma)': "#2BC6B6"

    @_setupCanvas()

    @_setupTitle()

    @_setupDetails()

    @_setupPlanetarySystem()

    @_setupCalculations()

    @_setupEvents()

    @_resize()

    @firstRenderDone = false

    if initialSystem
      @showPlanetarySystem(initialSystem)
    else
      @_render()

  _setupTitle: ->
    @starSystemName = null

    @systemTitle = document.createElement("h1")

    @systemTitle.style.position = "absolute"
    @systemTitle.style.fontSize = "xx-large"
    @systemTitle.style.color = "White"
    @systemTitle.style.top = "25px"
    @systemTitle.style.left = "25px"

    @container.appendChild @systemTitle

  _setupDetails: ->
    @scaleDetail = document.createElement("p")

    @scaleDetail.innerText = "Scale"

    @scaleDetail.style.position = "absolute"
    @scaleDetail.style.color = "White"
    @scaleDetail.style.top = "80px"
    @scaleDetail.style.left = "25px"

    @container.appendChild @scaleDetail

  _setupCanvas: ->
    @container = document.getElementById @containerId
    @canvas = document.createElement "canvas"

    @container.appendChild @canvas
    @container.style.backgroundColor = "black";

    @context = @canvas.getContext "2d"

  _setupPlanetarySystem: ->
    @currentOrbital = null

  _setupCalculations: ->
    @scaleCurrentExponent = 10
    @scaleCurrent = 0

    @viewPortX = 0
    @viewPortY = 0
    @viewPortScaledX = 0
    @viewPortScaledY = 0

    @zeroPointX = 0
    @zeroPointY = 0

    @mouseX = 0
    @mouseY = 0
    @mouseRealX = 0
    @mouseRealY = 0

  _setupEvents: ->
    @areWePanning = false
    @panningPrevPageX = 0
    @panningPrevPageY = 0

    @canvas.onmousedown = => @_handleMouseDown()
    @canvas.onmouseup = => @_handleMouseUp()
    @canvas.onmouseout = => @_handleMouseOut()
    @canvas.onmousemove = (evt) => @_handleMouseMove(evt)
    @canvas.onmousewheel = (evt) => @_handleMouseWheel(evt)

    @listeners = {}

  _updatePlanetarySystemName: ->
    @currentStarSystemName = @planetarySystem.id
    @systemTitle.innerHTML = @planetarySystem.id

  _updateDetails: ->
    exponentText = '-' + @scaleCurrentExponent

    @scaleDetail.innerHTML = 'Scale: 10' + exponentText.sup()

  _resetCalculations: ->
    @_setupCalculations()

    @_calculateScale()

    @_calculateViewPort()

  _calculateScale: ->
    @scaleCurrent = Math.pow(10, -@scaleCurrentExponent)

  _calculateViewPort: ->
    @viewPortScaledX = @viewPortX * @scaleCurrent
    @viewPortScaledY = @viewPortY * @scaleCurrent

    @zeroPointX = @centerX + @viewPortScaledX
    @zeroPointY = @centerY + @viewPortScaledY

  _resize: ->
    @canvas.width = @width
    @canvas.height = @height

    @centerX = @canvas.width / 2
    @centerY = @canvas.height / 2

  _checkForMouseItem: ->
    foundOrbital = null

    for orbital in @planetarySystem.star.planets
      orbitalX = -orbital.x
      orbitalY = -orbital.z
      bodyRadius = orbital.radius

      lowerLeftX = orbitalX - bodyRadius
      lowerLeftY = orbitalY - bodyRadius
      upperRightX = orbitalX + bodyRadius
      upperRightY = orbitalY + bodyRadius

      @mouseRealX = @viewPortX + ((@centerX - (@mouseX - $(@canvas).offset().left)) / @scaleCurrent)
      @mouseRealY = @viewPortY + ((@centerY - (@mouseY - $(@canvas).offset().top)) / @scaleCurrent)

      if lowerLeftX < @mouseRealX and
         lowerLeftY < @mouseRealY and
         upperRightX > @mouseRealX and
         upperRightY > @mouseRealY
        foundOrbital = orbital
        break

    if not foundOrbital and @currentOrbital
      @canvas.style.cursor = "inherit";
      @currentOrbital = null
      return true
    else if foundOrbital and not @currentOrbital
      @canvas.style.cursor = "pointer";
      @currentOrbital = foundOrbital
      return true

    return false

  _stopPanning: ->
    @areWePanning = false

    @panningPrevPageX = 0
    @panningPrevPageY = 0

  _handleMouseDown: ->
    @areWePanning = true

  _handleMouseUp: ->
    @_stopPanning()

    if @currentOrbital
      @_fireEvent("itemclick", @currentOrbital)

  _handleMouseOut: ->
    @_stopPanning()

  _handleMouseMove: (evt) ->
    @mouseX = evt.pageX;
    @mouseY = evt.pageY;
    @_fireEvent("debug", null)
    shouldRender = @_checkForMouseItem()

    if @areWePanning
      if @panningPrevPageX and @panningPrevPageY
        velX = evt.pageX - @panningPrevPageX
        velY = evt.pageY - @panningPrevPageY

        @moveViewPort(velX, velY)

      @panningPrevPageX = evt.pageX
      @panningPrevPageY = evt.pageY
    else
      @_render() if shouldRender

  _handleMouseWheel: (evt) ->
    scrollDelta = 1 / 16

    if evt.wheelDelta < 0
      @scaleExponentTo(@scaleCurrentExponent - scrollDelta)
    else
      @scaleExponentTo(@scaleCurrentExponent + scrollDelta)

  _fireEvent: (type, evt) ->
    if @listeners[type]
      for listener in @listeners[type]
        listener(evt)

  _render: ->
    @_fireEvent("debug", null)

    @_preDraw()

    @_drawStarSystem()

    @_postDraw()

  _preDraw: ->
    @_resize()

    @_updateDetails()

    @context.clearRect 0, 0, @canvas.width, @canvas.height

    @context.fillStyle = "#FFFFFF"
    @context.strokeStyle = "#FFFFFF"

  _drawOrbital: (parent, orbital) ->
    orbitalX = (orbital.x * @scaleCurrent) + @zeroPointX
    orbitalY = (orbital.z * @scaleCurrent) + @zeroPointY
    bodyRadius = orbital.radius * @scaleCurrent

    if not @firstRenderDone
      console.log "Drawing " + orbital.id + " at (" + orbitalX + ", " + orbitalY + ")"

    drawOrbit = false
    orbitCenterX = @zeroPointX
    orbitCenterY = @zeroPointY
    orbitRadius = Math.sqrt(Math.pow(orbital.x, 2) + Math.pow(orbital.z, 2)) * @scaleCurrent

    if orbital.class == 'Eve\\Model\\Planet' and @scaleCurrentExponent > 7.5
      drawOrbit = true
    if orbital.class == 'Eve\\Model\\Moon' and @scaleCurrentExponent < 7.5
      drawOrbit = true
      moonX = parent.x - orbital.x
      moonY = parent.z - orbital.z
      orbitCenterX = (parent.x * @scaleCurrent) + @zeroPointX
      orbitCenterY = (parent.z * @scaleCurrent) + @zeroPointY
      orbitRadius = Math.sqrt(Math.pow(moonX, 2) + Math.pow(moonY, 2)) * @scaleCurrent

    if drawOrbit
      @context.beginPath()
      @context.arc orbitCenterX, orbitCenterY, orbitRadius, 0, Math.PI * 2, false
      @context.closePath()
      @context.stroke()

    orbitalBodyColor = "#FF00FF";

    if @_planetColors[orbital.planet_type_name]
      orbitalBodyColor = @_planetColors[orbital.planet_type_name];

    @context.fillStyle = orbitalBodyColor;

    @context.beginPath()
    @context.arc orbitalX, orbitalY, bodyRadius, 0, Math.PI * 2
    @context.closePath()
    @context.fill()

    @context.font = "12px Verdana"

    if @currentOrbital and @currentOrbital.id == orbital.id
      @context.font = "16px Verdana"

    @context.fillStyle = "#FFFFFF"

    textMetrics = @context.measureText orbital.id

    @context.fillText orbital.id, orbitalX - (textMetrics.width / 2), orbitalY

  _drawStarSystem: ->
    # TODO: Stations, Stargates, etc
    #for planet in @planetarySystem.planets
    #  @_drawOrbital(null, celestial)

    for planet in @planetarySystem.star.planets
      @_drawOrbital(@planetarySystem.star, planet)
      for moon in planet.moons
        @_drawOrbital(planet, moon)

    return null

  _postDraw: ->
    @context.beginPath()
    @context.moveTo @centerX - 5, @centerY
    @context.lineTo @centerX + 5, @centerY
    @context.closePath()
    @context.stroke()

    @context.beginPath()
    @context.moveTo @centerX, @centerY - 5
    @context.lineTo @centerX, @centerY + 5
    @context.closePath()
    @context.stroke()

    @firstRenderDone = true

  addEventListener: (type, func) ->
    if not @listeners[type]
      @listeners[type] = []

    @listeners[type].push(func)

  removeEventListener: (type, func) ->
    for i in [0 .. @listeners.length]
      if @listeners[i] == func
        @listeners.splice i, 1

  moveViewPort: (x, y) ->
    @viewPortX += (x / @scaleCurrent)
    @viewPortY += (y / @scaleCurrent)

    @_calculateViewPort()

    @_render()

  scaleExponentTo: (s) ->
    @scaleCurrentExponent = s

    @_calculateScale()

    @_calculateViewPort()

    @_render()

    @_fireEvent("scalechange", @scaleCurrentExponent)

  centerOnPlanet: (planetname) ->
    for planet in @planetarySystem.star.planets
      if planet.id == planetname
        @viewPortX = -planet.x;
        @viewPortY = -planet.z;

        @_calculateViewPort()

        @_render()

        break
    return undefined

  showPlanetarySystem: (planetarySystemName) ->
    @_loadPlanetarySystemData(planetarySystemName)

  _loadPlanetarySystemData: (planetarySystemName) ->
    $.ajax
      url: '/planetary-system-explorer/' + planetarySystemName
      success: (data) =>
        console.log('Retrieved planetary system ' + data.id)

        @planetarySystem = data

        @_updatePlanetarySystemName()

        # TODO: Move this out of here
        @_resetCalculations()

        @_render()

    return undefined

  _loadStationData: ->
    return undefined