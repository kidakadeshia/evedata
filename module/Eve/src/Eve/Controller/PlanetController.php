<?php

namespace Eve\Controller;

class PlanetController extends StandardController
{
    public function __construct()
    {
        parent::__construct(array(
            'className' => '\Eve\Model\Planet',
            'route' => 'planet',
            'showRoute' => 'planet/show',
            'searchRoute' => 'planet/search',
            'indexRoute' => 'planet/index',
            'viewBasePath' => 'eve/planet/',
            'show' => array(
                'notFoundRouteName' => 'planet',
                'header' => 'eve/planet/show/header',
                'tabs' => array(
                    'locationTab' => array(
                        'partial' => 'eve/planet/show/location',
                        'tabName' => 'Location'
                    ),
                    'propertiesTab' => array(
                        'partial' => 'eve/planet/show/properties',
                        'tabName' => 'Properties'
                    ),
                )
            ),
            'index' => array(
                'orderBy' => 'name'
            ),
            'search' => array(
                'searchField' => 'name',
            )
        ));
    }
}

?>
