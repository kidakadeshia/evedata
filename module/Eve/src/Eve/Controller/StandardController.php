<?php

namespace Eve\Controller;

use Zend\View\Model\ViewModel;

abstract class StandardController extends BaseController
{
    private $className;
    private $showOptions;
    private $indexOptions;
    private $searchOptions;
    
    public function __construct($options)
    {
        parent::__construct();
        
        $this->className = $options['className'];
        
        $this->viewBasePath = $options['viewBasePath'];

        $this->route = $options['route'];
        $this->showRoute = $options['showRoute'];
        $this->searchRoute = $options['searchRoute'];
        $this->indexRoute = $options['indexRoute'];
        
        $this->showOptions = $options['show'];
        $this->indexOptions = $options['index'];
        $this->searchOptions = $options['search'];
    }

    public function getParameterFormat()
    {
        return $this->params('format');
    }

    public function getParameterId()
    {
        return $this->params('id');
    }
    
    public function showAction()
    {
        $id = $this->getParameterId();

        $entity = $this->em->getRepository($this->className)->find($id);

        if ($entity)
        {
            if ($this->getParameterFormat() == 'yml')
            {
                return new \Celaeno\Yaml\YamlModel($entity);
            }
            else if ($this->getParameterFormat() == 'json')
            {
                return new \Celaeno\JSON\JsonModel($entity);
            }

            $view = new ViewModel();
        
            $view->setTemplate('eve/standard/show');
            
            $view->setVariables(array(
               'entity' => $entity,
               'route' => $this->route,
               'header' => $this->showOptions["header"],
               'tabs' => $this->showOptions["tabs"],
               'pageTitlePartial' => $this->viewBasePath . 'show/title',
               'pageTitleArgs' => array(
                   'args' => isset($this->showOptions['pageTitleArgs']) ? $this->showOptions['pageTitleArgs'] : null,
                   'entity' => $entity
               )
            ));
            
            return $view;
        }
        else
        {
            return $this->redirect()->toRoute($this->showOptions["notFoundRouteName"]);
        }
    }
    
    public function indexAction()
    {
        $limit = $this->params('limit') > 0 ? $this->params('limit') : 1;
        $offset = $this->params('offset');
        
        $entityRepo = $this->em->getRepository($this->className);
     
        $entities = $entityRepo->findBy(array(),
                                       array($this->indexOptions['orderBy'] => 'ASC'),
                                       $limit, $offset);
        
        $length = $entityRepo->getTotal();
        
        $view = new ViewModel();
        
        $view->setTemplate('eve/standard/index');
        
        $view->setVariables(array(
            'result' => $entities,
            'limit' => $limit,
            'length' => $length,
            'offset' => $offset,
            'showRoute' => $this->showRoute,
            'indexRoute' => $this->indexRoute,
            'pageTitlePartial' => $this->viewBasePath . 'index/title',
            'pageTitleArgs' => array(
                'args' => isset($this->showOptions['pageTitleArgs']) ? $this->showOptions['pageTitleArgs'] : null,
            )
        ));
        
        return $view;
    }
    
    public function searchAction()
    {
        $criteria = $this->params('criteria');
        
        $view = new ViewModel();
        
        $view->setTemplate('eve/standard/search');

        $view->setVariables(array(
            'showRoute' => $this->showRoute,
            'searchRoute' => $this->searchRoute,
            'pageTitlePartial' => $this->viewBasePath . 'search/title',
            'pageTitleArgs' => array(
                'args' => isset($this->showOptions['pageTitleArgs']) ? $this->showOptions['pageTitleArgs'] : null,
            )
        ));
        
        if ($criteria !== null && strlen($criteria) >= 3)
        {
            $entityRepo = $this->em->getRepository($this->className);

            $results = $entityRepo->findLike(array($this->searchOptions['searchField'] => '%' . $criteria . '%'));

            if (count($results) > 1)
            {
                $view->setVariables(array(
                    'criteria' => $criteria,
                    'results' => $results,
                ));
                
                return $view;
            }
            else if (count($results) == 1)
            {
                $this->redirect()->toRoute($this->showRoute, array('id' => $results[0]->getId()));
            }
        }
        
        $view->setVariables(array(
            'criteria' => null,
            'results' => array(),
        ));

        return $view;
    }
}

?>
