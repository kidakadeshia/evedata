<?php

namespace Eve\Controller;

class FactionController extends StandardController
{
    public function __construct()
    {
        parent::__construct(array(
            'className' => '\Eve\Model\Faction',
            'route' => 'faction',
            'showRoute' => 'faction/show',
            'searchRoute' => 'faction/search',
            'indexRoute' => 'faction/index',
            'viewBasePath' => 'eve/faction/',
            'show' => array(
                'notFoundRouteName' => 'faction',
                'header' => 'eve/faction/show/header',
                'tabs' => null
            ),
            'index' => array(
                'orderBy' => 'name'
            ),
            'search' => array(
                'searchField' => 'name',
            )
        ));
    }
}

?>
