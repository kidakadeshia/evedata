<?php

namespace Eve\Controller;

class ConstellationController extends StandardController
{
    public function __construct()
    {
        parent::__construct(array(
            'className' => '\Eve\Model\Constellation',
            'route' => 'constellation',
            'showRoute' => 'constellation/show',
            'searchRoute' => 'constellation/search',
            'indexRoute' => 'constellation/index',
            'viewBasePath' => 'eve/constellation/',
            'show' => array(
                'notFoundRouteName' => 'constellation',
                'header' => 'eve/constellation/show/header',
                'tabs' => array(
                    'locationsTab' => array(
                        'partial' => 'eve/standard/show/locations',
                        'tabName' => 'Locations'
                    ),
                    'planetarySystemsTab' => array(
                        'partial' => 'eve/constellation/show/planetary-systems',
                        'tabName' => 'Planetary Systems'
                    ),
                    'connectedToTab' => array(
                        'criteria' => function ($entity) { return \count($entity->getConnectedConstellations()) > 0; },
                        'partial' => 'eve/constellation/show/connected-to',
                        'tabName' => 'Connected to'
                    ),
                )
            ),
            'index' => array(
                'orderBy' => 'name'
            ),
            'search' => array(
                'searchField' => 'name',
            )
        ));
    }
}

?>
