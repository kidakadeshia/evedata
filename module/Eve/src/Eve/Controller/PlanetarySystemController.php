<?php

namespace Eve\Controller;

class PlanetarySystemController extends StandardController
{
    public function __construct()
    {
        parent::__construct(array(
            'className' => '\Eve\Model\PlanetarySystem',
            'route' => 'planetary-system',
            'showRoute' => 'planetary-system/show',
            'searchRoute' => 'planetary-system/search',
            'indexRoute' => 'planetary-system/index',
            'viewBasePath' => 'eve/planetary-system/',
            'show' => array(
                'notFoundRouteName' => 'planetary-system',
                'header' => 'eve/planetary-system/show/header',
                'tabs' => array(
                    'locationsTab' => array(
                        'partial' => 'eve/standard/show/locations',
                        'tabName' => 'Locations'
                    ),
                    /*
                    'propertiesTab' => array(
                        'partial' => 'eve/planetary-system/show/properties',
                        'tabName' => 'Properties'
                    ),
                    */
                    'connectedToTab' => array(
                        'criteria' => function ($entity) { return \count($entity->getConnectedPlanetarySystems()) > 0; },
                        'partial' => 'eve/planetary-system/show/connected-to',
                        'tabName' => 'Connected to'
                    ),
                    'stationsTab' => array(
                        'criteria' => function ($entity) { return \count($entity->getStations()) > 0; },
                        'partial' => 'eve/planetary-system/show/stations',
                        'tabName' => 'Stations'
                    ),
                    'viewersTab' => array(
                        'partial' => 'eve/planetary-system/show/viewer',
                        'tabName' => 'Viewer'
                    )
                )
            ),
            'index' => array(
                'orderBy' => 'name'
            ),
            'search' => array(
                'searchField' => 'name',
            )
        ));
    }
}

?>
