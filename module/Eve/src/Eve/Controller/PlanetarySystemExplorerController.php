<?php

namespace Eve\Controller;

use Zend\View\Model\JsonModel;

class PlanetarySystemExplorerController extends BaseController
{
    function indexAction()
    {
        $planetarySystemName = $this->params('planetarysystemname');

        $ps_repo = $this->em->getRepository('Eve\Model\PlanetarySystem');

        $planetarySystem = $ps_repo->find($planetarySystemName);

        $serPlanetarySystem = $planetarySystem->serialize();

        $star = $planetarySystem->getStar();

        $serPlanetarySystem['star'] = $star->serialize();

        $planets = $star->getPlanets();

        $serPlanetarySystem['star']['planets'] = array();

        foreach ($planets as $planet)
        {
            $serPlanet = $planet->serialize();

            $serPlanet['moons'] = array();

            foreach ($planet->getMoons() as $moon)
            {
                $serMoon = $moon->serialize();

                $serPlanet['moons'][] = $serMoon;
            }

            $serPlanetarySystem['star']['planets'][] = $serPlanet;
        }

        return new JsonModel($serPlanetarySystem);
    }

}

?>
