<?php

namespace Eve\Controller;

class StationController extends StandardController
{
    public function __construct()
    {
        parent::__construct(array(
            'className' => '\Eve\Model\Station',
            'route' => 'station',
            'showRoute' => 'station/show',
            'searchRoute' => 'station/search',
            'indexRoute' => 'station/index',
            'viewBasePath' => 'eve/station/',
            'show' => array(
                'notFoundRouteName' => 'station',
                'header' => 'eve/station/show/header',
                'tabs' => array(
                    'locationTab' => array(
                        'partial' => 'eve/station/show/location',
                        'tabName' => 'Location'
                    ),
                    'reprocessingTab' => array(
                        'partial' => 'eve/station/show/reprocessing',
                        'tabName' => 'Reprocessing'
                    )
                )
            ),
            'index' => array(
                'orderBy' => 'name'
            ),
            'search' => array(
                'searchField' => 'name',
            )
        ));
    }
}

?>
