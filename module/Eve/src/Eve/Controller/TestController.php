<?php

namespace Eve\Controller;

use Celaeno\Form\Form;
use Celaeno\Form\TextWidget;
use Zend\View\Model\ViewModel;

class TestController extends BaseController
{
    public function setupView()
    {
        $view = new ViewModel();

        $view->setVariable('pageTitlePartial', 'eve/test/title.phtml');

        return $view;
    }

    public function setupForm()
    {
        $form = new Form('test_form');

        $form->addWidget(new TextWidget('test'));
        $form->addWidget(new TextWidget('bloep'));

        return $form;
    }

    public function formsAction()
    {
        $view = $this->setupView();

        $form = $this->setupForm();

        $view->setVariable('formContent', $form->render());

        return $view;
    }
}