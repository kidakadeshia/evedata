<?php

namespace Eve\Model;

use Doctrine\ORM\EntityManager;

/**
 * @Entity(repositoryClass="\Eve\Model\Repositories\ConstellationRepository")
 * @Table(name="constellations")
 **/
class Constellation extends \Celaeno\ORM\Model
{
    /**
     * @Id
     * @Column(type="string")
     **/
    protected $name;

    /** @Column(type="integer") **/
    protected $original_id;

    /**
     * @OneToOne(targetEntity="Faction")
     * @JoinColumn(name="faction_name", referencedColumnName="name")
     **/
    protected $faction;

    /**
     * @OneToOne(targetEntity="Region")
     * @JoinColumn(name="region_name", referencedColumnName="name")
     **/
    protected $region;

    /**
     * @OneToMany(targetEntity="PlanetarySystem", mappedBy="constellation")
     */
    protected $planetary_systems;

    /**
     * @ManyToMany(targetEntity="Constellation")
     * @JoinTable(name="connected_constellations",
     *      joinColumns={@JoinColumn(name="constellation_name", referencedColumnName="name")},
     *      inverseJoinColumns={@JoinColumn(name="connected_constellation_name", referencedColumnName="name")}
     *      )
     */
    protected $connected_constellations;

    /** @Column(type="float") **/
    protected $x;

    /** @Column(type="float") **/
    protected $y;

    /** @Column(type="float") **/
    protected $z;

    /** @Column(type="float") **/
    protected $x_minimum;

    /** @Column(type="float") **/
    protected $x_maximum;

    /** @Column(type="float") **/
    protected $y_minimum;

    /** @Column(type="float") **/
    protected $y_maximum;

    /** @Column(type="float") **/
    protected $z_minimum;

    /** @Column(type="float") **/
    protected $z_maximum;

    /** @Column(type="float") **/
    protected $radius;

    /** @Column(type="DateTimeMs") **/
    protected $created_on;

    /** @Column(type="DateTimeMs") **/
    protected $updated_on;

    public function getId()
    {
        return $this->name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOriginalId()
    {
        return $this->original_id;
    }

    public function getFaction()
    {
        return $this->faction;
    }

    public function setFaction(Faction $faction)
    {
        $this->faction = $faction;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion(Region $region)
    {
        $this->region = $region;
    }

    public function getPlanetarySystems()
    {
        return $this->planetary_systems;
    }

    public function getConnectedConstellations()
    {
        return $this->connected_constellations;
    }

    public function getX()
    {
        return $this->x;
    }

    public function getY()
    {
        return $this->y;
    }

    public function getZ()
    {
        return $this->z;
    }

    public function getXMinimum()
    {
        return $this->x_minimum;
    }

    public function getXMaximum()
    {
        return $this->x_maximum;
    }

    public function getYMinimum()
    {
        return $this->y_minimum;
    }

    public function getYMaximum()
    {
        return $this->y_maximum;
    }

    public function getZMinimum()
    {
        return $this->z_minimum;
    }

    public function getZMaximum()
    {
        return $this->z_maximum;
    }

    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }

    protected function getSerializedMapping()
    {
        return [
            'class' => __CLASS__,
            'fields' => [
                ['fieldName' => 'faction'],
                ['fieldName' => 'region'],
                ['fieldName' => 'planetary_systems'],
                ['fieldName' => 'connected_constellations'],
                ['fieldName' => 'x'],
                ['fieldName' => 'y'],
                ['fieldName' => 'z'],
                ['fieldName' => 'xMinimum'],
                ['fieldName' => 'yMinimum'],
                ['fieldName' => 'zMinimum'],
                ['fieldName' => 'xMaximum'],
                ['fieldName' => 'yMaximum'],
                ['fieldName' => 'zMaximum'],
                ['fieldName' => 'radius'],
            ]
        ];
    }

    public function __toString()
    {
        return '<' . $this->name . ' - ' . $this->region->getName() . '>';
    }
}

?>
