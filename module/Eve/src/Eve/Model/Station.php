<?php

namespace Eve\Model;

/**
 * @Entity(repositoryClass="\Eve\Model\Repositories\StationRepository")
 * @Table(name="stations")
 **/
class Station extends \Celaeno\ORM\Model
{
    /**
     * @Id
     * @Column(type="string")
     **/
    protected $name;

    /** @Column(type="integer") **/
    protected $original_id;

    /** @Column(type="string") **/
    protected $operation_name;

    /**
     * TODO: Find a better way for this. Doctrine really sucks right now.
     * @ManyToMany(targetEntity="PlanetarySystem")
     * @JoinTable(name="station_planetary_systems",
     *      joinColumns={
     *          @JoinColumn(name="station_name", referencedColumnName="name")
     *      },
     *      inverseJoinColumns={
     *          @JoinColumn(name="planetary_system_name", referencedColumnName="name")
     *      }
     * )
     **/
    protected $planetary_system;

    /**
     * @OneToOne(targetEntity="Corporation")
     * @JoinColumn(name="corporation_name", referencedColumnName="name")
     **/
    protected $corporation;
    
    /** @Column(type="integer") **/
    protected $security;
    
    /** @Column(type="float") **/
    protected $docking_cost_per_volume;
    
    /** @Column(type="float") **/
    protected $max_ship_volume_dockable;

    /** @Column(type="integer") **/
    protected $office_rental_cost;

    /** @Column(type="float") **/
    protected $x;
    
    /** @Column(type="float") **/
    protected $y;
    
    /** @Column(type="float") **/
    protected $z;
    
    /** @Column(type="float") **/
    protected $reprocessing_efficiency;
    
    /** @Column(type="float") **/
    protected $reprocessing_stations_take;
    
    /** @Column(type="integer") **/
    protected $reprocessing_hangar_flag;

    /** @Column(type="DateTimeMs") **/
    protected $created_on;

    /** @Column(type="DateTimeMs") **/
    protected $updated_on;

    public function getId()
    {
        return $this->name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOriginalId()
    {
        return $this->original_id;
    }

    public function getPlanetarySystem()
    {
        // TODO: Fuck Doctrine.
        return $this->planetary_system[0];
    }

    public function getCorporation()
    {
        return $this->corporation;
    }

    public function setCorporation(Corporation $corporation)
    {
        $this->corporation = $corporation;
    }

    public function getSecurity()
    {
        return $this->security;
    }
    
    public function getDockingCostPerVolume()
    {
        return $this->docking_cost_per_volume;
    }
    
    public function getMaxShipVolumeDockable()
    {
        return $this->max_ship_volume_dockable;
    }
    
    public function getOfficeRentalCost()
    {
        return $this->office_rental_cost;
    }

    public function getX()
    {
        return $this->x;
    }
    
    public function getY()
    {
        return $this->y;
    }
    
    public function getZ()
    {
        return $this->z;
    }
    
    public function getReprocessingEfficiency()
    {
        return $this->reprocessing_efficiency;
    }
    
    public function getReprocessingStationsTake()
    {
        return $this->reprocessing_stations_take;
    }
    
    public function getReprocessingHangarFlag()
    {
        return $this->reprocessing_hangar_flag;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }

    protected function getSerializedMapping()
    {
        return [
            'class' => __CLASS__,
            'fields' => [
                ['fieldName' => 'planetary_system'],
                ['fieldName' => 'corporation'],
                ['fieldName' => 'security'],
                ['fieldName' => 'docking_cost_per_volume'],
                ['fieldName' => 'max_ship_volume_dockable'],
                ['fieldName' => 'office_rental_cost'],
                ['fieldName' => 'x'],
                ['fieldName' => 'y'],
                ['fieldName' => 'z'],
                ['fieldName' => 'reprocessing_efficiency'],
                ['fieldName' => 'reprocessing_stations_take'],
                ['fieldName' => 'reprocessing_hangarflag'],
            ]
        ];
    }

    public function __toString()
    {
        return '<'
            . $this->name
            . ' - '
            . $this->planetary_system->getName()
            . ' - '
            . $this->planetary_system->getConstellation()->getName()
            . ' - '
            . $this->planetary_system->getConstellation()->getRegion()->getName()
        . '>';
    }
}

?>
