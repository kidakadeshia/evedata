<?php

namespace Eve\Model;

/**
 * @Entity(repositoryClass="\Eve\Model\Repositories\CorporationInvestorRepository")
 * @Table(name="corporation_investors")
 **/
class CorporationInvestor
{
    /**
     * @Id
     * @OneToOne(targetEntity="Corporation")
     * @JoinColumn(name="corporation_name", referencedColumnName="name")
     **/
    protected $corporation;

    /**
     * @Id
     * @OneToOne(targetEntity="Corporation")
     * @JoinColumn(name="investor_corporation_name", referencedColumnName="name")
     **/
    protected $investor_corporation;

    /** @Column(type="integer") **/
    protected $shares;

    public function getCorporation()
    {
        return $this->corporation;
    }

    public function setCorporation(Corporation $corporation)
    {
        $this->corporation = $corporation;
    }

    public function getInvestorCorporation()
    {
        return $this->investor_corporation;
    }

    public function setInvestorCorporation(Corporation $investor_corporation)
    {
        $this->investor_corporation = $investor_corporation;
    }

    public function getShares()
    {
        return $this->shares;
    }

    public function __toString()
    {
        return '<'
            . $this->corporation->getName()
            . ' - '
            . $this->investor_corporation->getName()
            . ' - '
            . $this->shares
        . '>';
    }
}