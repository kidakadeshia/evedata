<?php

namespace Eve\Model;

/**
 * @Entity(repositoryClass="\Eve\Model\Repositories\CorporationRepository")
 * @Table(name="corporations")
 **/
class Corporation extends \Celaeno\ORM\Model
{
    /**
     * @Id
     * @Column(type="string")
     **/
    protected $name;

    /** @Column(type="integer") **/
    protected $original_id;

    /**
     * @OneToOne(targetEntity="PlanetarySystem")
     * @JoinColumn(name="planetary_system_name", referencedColumnName="name")
     **/
    protected $planetary_system;

    /**
     * @OneToOne(targetEntity="Faction")
     * @JoinColumn(name="faction_name", referencedColumnName="name")
     **/
    protected $faction;

    /**
     * @OneToOne(targetEntity="Corporation")
     * @JoinColumn(name="friend_corporation_name", referencedColumnName="name")
     **/
    protected $friend_corporation;

    /**
     * @OneToOne(targetEntity="Corporation")
     * @JoinColumn(name="enemy_corporation_name", referencedColumnName="name")
     **/
    protected $enemy_corporation;

    /** @Column(type="string") **/
    protected $size;
    
    /** @Column(type="string") **/
    protected $extent;
    
    /** @Column(type="integer") **/
    protected $public_shares;
    
    /** @Column(type="integer") **/
    protected $initial_price;
    
    /** @Column(type="float") **/
    protected $min_security;
    
    /** @Column(type="boolean") **/
    protected $scattered;

    /** @Column(type="float") **/
    protected $size_factor;
    
    /** @Column(type="string") **/
    protected $description;

    /** @Column(type="DateTimeMs") **/
    protected $created_on;

    /** @Column(type="DateTimeMs") **/
    protected $updated_on;

    public function getId()
    {
        return $this->name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOriginalId()
    {
        return $this->original_id;
    }

    public function getPlanetarySystem()
    {
        return $this->planetary_system;
    }

    public function setPlanetarySystem(PlanetarySystem $planetary_system)
    {
        $this->planetary_system = $planetary_system;
    }

    public function getFaction()
    {
        return $this->faction;
    }

    public function setFaction(Faction $faction)
    {
        $this->faction = $faction;
    }

    public function getFriendCorporation()
    {
        return $this->friend_corporation;
    }

    public function setFriendCorporation(Corporation $friend_corporation)
    {
        $this->friend_corporation = $friend_corporation;
    }

    public function getEnemyCorporation()
    {
        return $this->enemy_corporation;
    }

    public function setEnemyCorporation(Corporation $enemy_corporation)
    {
        $this->enemy_corporation = $enemy_corporation;
    }

    public function getSize()
    {
        return $this->size;
    }
    
    public function getExtent()
    {
        return $this->extent;
    }
    
    public function getPublicShares()
    {
        return $this->public_shares;
    }
    
    public function getInitialPrice()
    {
        return $this->initial_price;
    }
    
    public function getMinSecurity()
    {
        return $this->min_security;
    }
    
    public function isScattered()
    {
        return $this->scattered;
    }

    public function getSizeFactor()
    {
        return $this->size_factor;
    }
    
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }

    protected function getSerializedMapping()
    {
        return [
            'class' => __CLASS__,
            'fields' => [
                ['fieldName' => 'planetary_system'],
                ['fieldName' => 'faction'],
                ['fieldName' => 'friend_corporation'],
                ['fieldName' => 'enemy_corporation'],
                ['fieldName' => 'size'],
                ['fieldName' => 'extent'],
                ['fieldName' => 'public_shares'],
                ['fieldName' => 'initial_price'],
                ['fieldName' => 'min_security'],
                ['fieldName' => 'is_scattered', 'methodName' => 'isScattered'],
                ['fieldName' => 'size_factor'],
                ['fieldName' => 'description'],
            ]
        ];
    }

    public function __toString()
    {
        return '<' . $this->name . '>';
    }
}

?>
