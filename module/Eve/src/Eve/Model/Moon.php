<?php

namespace Eve\Model;

/**
 * @Entity(repositoryClass="\Eve\Model\Repositories\MoonRepository")
 * @Table(name="moons")
 **/
class Moon extends \Celaeno\ORM\Model
{
    /**
     * @Id
     * @Column(type="string")
     **/
    protected $name;

    /** @Column(type="integer") **/
    protected $original_id;

    /**
     * @OneToOne(targetEntity="Planet")
     * @JoinColumn(name="planet_name", referencedColumnName="name")
     **/
    protected $planet;

    /** @Column(type="float") **/
    protected $x;

    /** @Column(type="float") **/
    protected $y;

    /** @Column(type="float") **/
    protected $z;

    /** @Column(type="float") **/
    protected $radius;

    /** @Column(type="float") **/
    protected $temperature;

    /** @Column(type="float") **/
    protected $orbit_radius;

    /** @Column(type="float") **/
    protected $orbit_period;

    /** @Column(type="float") **/
    protected $eccentricity;

    /** @Column(type="gmp_number") **/
    protected $mass_dust;

    /** @Column(type="gmp_number") **/
    protected $mass_gas;

    /** @Column(type="float") **/
    protected $density;

    /** @Column(type="float") **/
    protected $surface_gravity;

    /** @Column(type="float") **/
    protected $escape_velocity;

    /** @Column(type="float") **/
    protected $rotation_rate;

    /** @Column(type="boolean") **/
    protected $locked;

    /** @Column(type="float") **/
    protected $pressure;

    /** @Column(type="integer") **/
    protected $orbit_position;

    /** @Column(type="DateTimeMs") **/
    protected $created_on;

    /** @Column(type="DateTimeMs") **/
    protected $updated_on;

    public function getId()
    {
        return $this->name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOriginalId()
    {
        return $this->original_id;
    }

    public function getPlanet()
    {
        return $this->planet;
    }

    public function getX()
    {
        return $this->x;
    }

    public function getY()
    {
        return $this->y;
    }

    public function getZ()
    {
        return $this->z;
    }

    public function getRadius()
    {
        return $this->radius;
    }

    public function getTemperature()
    {
        return $this->temperature;
    }

    public function getOrbitRadius()
    {
        return $this->orbit_radius;
    }

    public function getOrbitPeriod()
    {
        return $this->orbit_period;
    }

    public function getEccentricity()
    {
        return $this->eccentricity;
    }

    public function getMassDust()
    {
        return $this->mass_dust;
    }

    public function getMassGas()
    {
        return $this->mass_gas;
    }

    public function getDensity()
    {
        return $this->density;
    }

    public function getSurfaceGravity()
    {
        return $this->surface_gravity;
    }

    public function getEscapeVelocity()
    {
        return $this->escape_velocity;
    }

    public function getRotationRate()
    {
        return $this->rotation_rate;
    }

    public function isLocked()
    {
        return $this->locked;
    }

    public function getPressure()
    {
        return $this->pressure;
    }

    public function getOrbitPosition()
    {
        return $this->orbit_position;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }

    protected function getSerializedMapping()
    {
        return [
            'class' => __CLASS__,
            'fields' => [
                ['fieldName' => 'planet'],
                ['fieldName' => 'x'],
                ['fieldName' => 'y'],
                ['fieldName' => 'z'],
                ['fieldName' => 'radius'],
                ['fieldName' => 'temperature'],
                ['fieldName' => 'orbit_radius'],
                ['fieldName' => 'orbit_period'],
                ['fieldName' => 'eccentricity'],
                ['fieldName' => 'mass_dust'],
                ['fieldName' => 'mass_gas'],
                ['fieldName' => 'density'],
                ['fieldName' => 'surface_gravity'],
                ['fieldName' => 'escape_velocity'],
                ['fieldName' => 'rotation_rate'],
                ['fieldName' => 'is_locked', 'methodName' => 'isLocked'],
                ['fieldName' => 'pressure'],
                ['fieldName' => 'orbit_position'],
            ]
        ];
    }

    public function __toString()
    {
        return '<'
            . $this->name
            . ' - '
            . $this->planet->getName()
            . ' - '
            . $this->planet->getStar()->getName()
            . ' - '
            . $this->planet->getStar()->getPlanetarySystem()->getName()
            . ' - '
            . $this->planet->getStar()->getPlanetarySystem()->getConstellation()->getName()
            . ' - '
            . $this->planet->getStar()->getPlanetarySystem()->getConstellation()->getRegion()->getName()
            . '>';
    }
}