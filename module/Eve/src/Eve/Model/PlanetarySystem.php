<?php

namespace Eve\Model;

/**
 * @Entity(repositoryClass="\Eve\Model\Repositories\PlanetarySystemRepository")
 * @Table(name="planetary_systems")
 **/
class PlanetarySystem extends \Celaeno\ORM\Model
{
    /**
     * @Id
     * @Column(type="string")
     **/
    protected $name;

    /** @Column(type="integer") **/
    protected $original_id;

    /**
     * @OneToOne(targetEntity="Constellation")
     * @JoinColumn(name="constellation_name", referencedColumnName="name")
     **/
    protected $constellation;

    /** @Column(type="string") **/
    protected $sun_type_name;

    /**
     * @OneToOne(targetEntity="Faction")
     * @JoinColumn(name="faction_name", referencedColumnName="name")
     **/
    protected $faction;

    /**
     * @ManyToMany(targetEntity="PlanetarySystem")
     * @JoinTable(name="planetary_system_jumps",
     *      joinColumns={@JoinColumn(name="from_planetary_system_name", referencedColumnName="name")},
     *      inverseJoinColumns={@JoinColumn(name="to_planetary_system_name", referencedColumnName="name")}
     *      )
     */
    protected $connected_planetary_systems;

    /**
     * @ManyToMany(targetEntity="Station")
     * @JoinTable(name="station_planetary_systems",
     *      joinColumns={
     *          @JoinColumn(name="planetary_system_name", referencedColumnName="name")
     *      },
     *      inverseJoinColumns={
     *          @JoinColumn(name="station_name", referencedColumnName="name")
     *      }
     * )
     */
    protected $stations;

    /**
     * @OneToOne(targetEntity="Star", mappedBy="planetary_system")
     */
    protected $star;

    /** @Column(type="float") **/
    protected $x;

    /** @Column(type="float") **/
    protected $y;

    /** @Column(type="float") **/
    protected $z;

    /** @Column(type="float") **/
    protected $x_minimum;

    /** @Column(type="float") **/
    protected $x_maximum;

    /** @Column(type="float") **/
    protected $y_minimum;

    /** @Column(type="float") **/
    protected $y_maximum;

    /** @Column(type="float") **/
    protected $z_minimum;

    /** @Column(type="float") **/
    protected $z_maximum;
    
    /** @Column(type="float") **/
    protected $luminosity;
    
    /** @Column(type="float") **/
    protected $security;
    
    /** @Column(type="float") **/
    protected $radius;
    
    /** @Column(type="string") **/
    protected $security_class;

    /** @Column(type="DateTimeMs") **/
    protected $created_on;

    /** @Column(type="DateTimeMs") **/
    protected $updated_on;

    public function getId()
    {
        return $this->name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOriginalId()
    {
        return $this->original_id;
    }

    public function getConstellation()
    {
        return $this->constellation;
    }

    public function setConstellation(Constellation $constellation)
    {
        $this->constellation = $constellation;
    }

    public function getFaction()
    {
        return $this->faction;
    }

    public function setFaction(Faction $faction)
    {
        $this->faction = $faction;
    }

    public function getConnectedPlanetarySystems()
    {
        return $this->connected_planetary_systems;
    }

    public function getChildNodes()
    {
        return $this->getConnectedPlanetarySystems();
    }

    public function getStations()
    {
        return $this->stations;
    }

    public function getStar()
    {
        return $this->star;
    }

    public function getX()
    {
        return $this->x;
    }

    public function getY()
    {
        return $this->y;
    }

    public function getZ()
    {
        return $this->z;
    }

    public function getXMinimum()
    {
        return $this->x_minimum;
    }

    public function getXMaximum()
    {
        return $this->x_maximum;
    }

    public function getYMinimum()
    {
        return $this->y_minimum;
    }

    public function getYMaximum()
    {
        return $this->y_maximum;
    }

    public function getZMinimum()
    {
        return $this->z_minimum;
    }

    public function getZMaximum()
    {
        return $this->z_maximum;
    }

    public function getSecurity()
    {
        return $this->security;
    }

    public function getRadius()
    {
        return $this->radius;
    }
    
    public function getSecurityClass()
    {
        return $this->security_class;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }

    protected function getSerializedMapping()
    {
        return [
            'class' => __CLASS__,
            'fields' => [
                ['fieldName' => 'constellation'],
                ['fieldName' => 'faction'],
                ['fieldName' => 'connected_planetary_systems'],
                ['fieldName' => 'stations'],
                ['fieldName' => 'star'],
                ['fieldName' => 'x'],
                ['fieldName' => 'y'],
                ['fieldName' => 'z'],
                ['fieldName' => 'xMinimum'],
                ['fieldName' => 'yMinimum'],
                ['fieldName' => 'zMinimum'],
                ['fieldName' => 'xMaximum'],
                ['fieldName' => 'yMaximum'],
                ['fieldName' => 'zMaximum'],
                ['fieldName' => 'security'],
                ['fieldName' => 'radius'],
                ['fieldName' => 'security_class'],
            ]
        ];
    }

    public function __toString()
    {
        return $this->name;
    }
}

?>
