<?php

namespace Eve\Model;

/**
 * @Entity(repositoryClass="\Eve\Model\Repositories\BaseRepository")
 * @Table(name="items")
 **/
class Item extends \Celaeno\ORM\Model
{
    /**
     * @Id
     * @Column(type="string")
     **/
    protected $name;

    /** @Column(type="integer") **/
    protected $original_id;

    /** @Column(type="string") **/
    protected $race_name;

    /** @Column(type="string") **/
    protected $market_group_name;

    /** @Column(type="string") **/
    protected $description;

    /** @Column(type="float") **/
    protected $mass;

    /** @Column(type="float") **/
    protected $volume;

    /** @Column(type="float") **/
    protected $capacity;

    /** @Column(type="integer") **/
    protected $portion_size;

    /** @Column(type="integer") **/
    protected $base_price;

    /** @Column(type="boolean") **/
    protected $published;

    /** @Column(type="float") **/
    protected $chance_of_duplicating;

    /** @Column(type="DateTimeMs") **/
    protected $created_on;

    /** @Column(type="DateTimeMs") **/
    protected $updated_on;

    public function getId()
    {
        return $this->name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOriginalId()
    {
        return $this->original_id;
    }

    public function getRace()
    {
        return $this->race_name;
    }

    public function setRace($race)
    {
        $this->race_name = $race;
    }

    public function getMarketGroup()
    {
        return $this->market_group_name;
    }

    public function setMarketGroup($market_group)
    {
        $this->market_group_name = $market_group;
    }
    
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }

    protected function getSerializedMapping()
    {
        return [
            'class' => __CLASS__,
            'fields' => [
                ['fieldName' => 'race'],
                ['fieldName' => 'market_group'],
                ['fieldName' => 'description'],
            ]
        ];
    }

    public function __toString()
    {
        return '<' . $this->name . '>';
    }
}

?>
