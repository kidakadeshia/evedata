<?php

namespace Eve\Model\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;

class BaseRepository extends EntityRepository
{
    private $tableName = null;
    private $entityName = null;
    private $className = null;
    
    public function __construct($em, ClassMetadata $class)
    {
        parent::__construct($em, $class);
        
        $this->className = $this->getClassName();
        $this->entityName = $this->getEntityName();
        $this->tableName = $class->getTableName();
    }
    
    public function getTotal()
    {
        $sql = "SELECT COUNT(*) FROM " . $this->tableName;
        
        $query = $this->getEntityManager()
                      ->getConnection()
                      ->executeQuery($sql);
        
        $query->execute();
        
        return $query->fetchColumn(0);
    }
    
    public function findLike($criteria)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $qb->select('ent')
           ->from($this->className, 'ent');
        
        foreach ($criteria as $name => $value)
        {
            $parameterName = 'prm_' . $name;
            
            $qb->andWhere($qb->expr()->like('ent.' . $name, ':' . $parameterName));
            
            $qb->setParameter($parameterName, $value);
        }
        
        return $qb->getQuery()->getResult();
    }
}

?>
