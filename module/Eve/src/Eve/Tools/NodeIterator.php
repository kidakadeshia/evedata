<?php

namespace Eve\Tools;

class NodeIterator
{
    protected $initial_node;

    protected $depth;

    protected $unvisited;

    protected $visited;

    protected $node_paths;

    protected $criteria_function;

    public function __construct($initial_node)
    {
        $this->initial_node = $initial_node;

        $this->continue = true;
        $this->depth = 0;
        $this->unvisited = null;
        $this->visited = null;
        $this->node_paths = null;
        $this->criteria_function = null;
    }

    protected function setupSets()
    {
        $this->unvisited = array();
        $this->visited = array();
        $this->node_paths = array();
    }

    public function run()
    {
        $this->setupSets();

        $this->visitNode($this->initial_node);

        while ($this->continue)
        {
            if (empty($this->unvisited))
            {
                break;
            }

            $this->increaseDepth();

            $to_be_visited = $this->unvisited;

            foreach ($to_be_visited as $node)
            {
                $this->visitNode($node);
            }

            $this->dumpInfo();
        }

        return $this->visited;
    }

    protected function visitNode($node)
    {
        $child_nodes = $node->getChildNodes();

        foreach ($child_nodes as $child_node)
        {
            if ($this->shouldIVisit($child_node))
            {
                $this->unvisited[] = $child_node;
                $this->unvisited = array_unique($this->unvisited);

                $this->node_paths[$child_node->getId()] = $node->getId();
            }
        }

        $this->setVisited($node);
    }

    protected function shouldIVisit($node)
    {
        if (in_array($node, $this->unvisited) || in_array($node, $this->visited))
        {
            return false;
        }

        //if ($this->criteria_function !== null)
        //{
        //    $criteria_function = $this->criteria_function;
        //    return $criteria_function($node, $this->depth + 1);
        //}

        return true;
    }

    protected function setVisited($node)
    {
        $key = array_search($node, $this->unvisited);

        if ($key !== false)
        {
            unset($this->unvisited[$key]);
        }

        $this->visited[] = $node;
        $this->visited = array_unique($this->visited);
    }

    protected function increaseDepth()
    {
        $this->depth++;
    }

    public function setCriteriaFunction($criteria_function)
    {
        $this->criteria_function = $criteria_function;
    }

    public function getVisited()
    {
        return $this->visited;
    }

    public function getUnvisited()
    {
        return $this->unvisited;
    }

    public function getNodePaths()
    {
        return $this->node_paths;
    }

    public function dumpInfo()
    {
        echo '==========================================================' . PHP_EOL;
        echo 'Node Paths Size: ' . count($this->node_paths) . PHP_EOL;
        echo 'Unvisited Size: ' . count($this->unvisited) . PHP_EOL;
        echo 'Visited Size: ' . count($this->visited) . PHP_EOL;
        echo 'Memory Usage (MB): ' . number_format(memory_get_usage() / 1024 / 1024, 4) . PHP_EOL;
    }
}