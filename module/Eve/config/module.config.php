<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'default' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'index',
                        'action' => 'index'
                    ),
                ),
            ),
            // This route is used to generate all the routes. See Module.php.
            // It is deleted afterward.
            'template' => array(
                'type' => 'literal',
                'may_terminate' => 'false',
                'options' => array(
                    'route' => null, // name
                    'defaults' => array(
                        'controller' => null, // name
                        'action' => 'index',
                        'limit' => 10,
                        'offset' => 0,
                    )
                ),
                'child_routes' => array(
                    'index' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/index[/:offset[/:limit]]',
                            'constraints' => array(
                                'limit' => '[0-9]*',
                                'offset' => '[0-9]*',
                            ),
                            'defaults' => array(
                                'limit' => 10,
                                'offset' => 0,
                            ),
                        ),
                    ),
                    'show' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/show[.:format]/:id',
                            'constraints' => array(
                                //'id' => '[a-zA-Z0-9\-_\s]*',
                            ),
                            'defaults' => array(
                                'action' => 'show'
                            ),
                        ),
                    ),
                    'search' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/search[/:criteria]',
                            'constraints' => array(
                                'criteria' => '[a-zA-Z0-9\-_\s%]*',
                            ),
                            'defaults' => array(
                                'criteria' => '',
                                'action' => 'search'
                            )
                         )
                    )
                ),
            ),
            'planetary-system-explorer' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/planetary-system-explorer[/:planetarysystemname]',
                    'constraints' => array(
                        //'planetarysystemname' => '[a-z]'
                    ),
                    'defaults' => array(
                        'controller' => 'planetary-system-explorer',
                        'action' => 'index',
                        'planetarysystemname' => 'No name'
                    )
                )
            ),
            'test' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/test/:action',
                    'defaults' => array(
                        'controller' => 'test'
                    )
                )
            )
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'YamlRenderer' => 'Celaeno\Yaml\YamlRendererFactory',
            'YamlStrategy' => 'Celaeno\Yaml\YamlStrategyFactory',
            'JsonRenderer' => 'Celaeno\JSON\JsonRendererFactory',
            'JsonStrategy' => 'Celaeno\JSON\JsonStrategyFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'index' => 'Eve\Controller\IndexController',
            'region' => 'Eve\Controller\RegionController',
            'planetary-system' => 'Eve\Controller\PlanetarySystemController',
            'constellation' => 'Eve\Controller\ConstellationController',
            'station' => 'Eve\Controller\StationController',
            'corporation' => 'Eve\Controller\CorporationController',
            'faction' => 'Eve\Controller\FactionController',
            'star' => 'Eve\Controller\StarController',
            'planet' => 'Eve\Controller\PlanetController',
            'moon' => 'Eve\Controller\MoonController',
            'planetary-system-explorer' => 'Eve\Controller\PlanetarySystemExplorerController',
            'test' => 'Eve\Controller\TestController'
        ),
        'public' => array(
            'region',
            'planetary-system',
            'constellation',
            'station',
            'corporation',
            'faction',
            'star',
            'planet',
            'moon'
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'eve/index/index' => __DIR__ . '/../view/eve/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
            'JsonStrategy',
            'YamlStrategy'
        ),
    ),
);
