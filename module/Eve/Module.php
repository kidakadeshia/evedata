<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eve;

use Zend\Mvc\Router\Http;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Celaeno\ORM\Types\DateTimeMsType;
use Celaeno\ORM\Types\GmpNumberType;

class Module
{
    /** @var \Doctrine\ORM\EntityManager */
    private static $em = null;

    private $config = null;

    public static function getDatabaseConfig()
    {
        return \Spyc::YAMLLoad(__DIR__ . '/config/database.yml');
    }

    public static function getBaseDirectory()
    {
        return __DIR__;
    }

    public static function getEntityManager()
    {
        if (Module::$em == null)
        {
            $dbconf = self::getDatabaseConfig();
            
            $modelLocation = __DIR__ . $dbconf["modelLocation"];
            
            $isDevMode = (bool)$dbconf["isDevMode"];

            $config = Setup::createAnnotationMetadataConfiguration(
                    array($modelLocation), 
                    $isDevMode);
            
            $config->setProxyDir(__DIR__ . $dbconf["proxyLocation"]);
            
            $config->setAutoGenerateProxyClasses($isDevMode);
            
            $conn = $dbconf["connection"];
            
            Module::$em = EntityManager::create($conn, $config);

            Module::registerCustomDoctrineTypes();
        }
        
        return Module::$em;
    }

    public function onBootstrap(MvcEvent $e)
    {
        $e->getApplication()->getServiceManager()->get('translator');
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    private static function registerCustomDoctrineTypes()
    {
        DateTimeMsType::registerType(Module::$em->getConnection());
        GmpNumberType::registerType(Module::$em->getConnection());
    }

    public function getConfig()
    {
        if (null === $this->config)
        {
            $this->config = include __DIR__ . '/config/module.config.php';

            $this->generateControllerRoutes();
        }

        return $this->config;
    }

    public function generateControllerRoutes()
    {
        $routes = &$this->config["router"]["routes"];
        $controllers = $this->config["controllers"]["public"];
        $template = $routes["template"];

        unset($routes["template"]);

        foreach ($controllers as $name)
        {
            $route = $template;

            $route["options"]["route"] = '/' . $name;
            $route["options"]["defaults"]["controller"] = $name;

            $routes[$name] = $route;
        }
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                ),
            ),
        );
    }
}
