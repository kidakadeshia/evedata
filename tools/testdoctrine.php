<?php

chdir(dirname(__DIR__));

// Setup autoloading

require_once 'init_autoloader.php';

Zend\Mvc\Application::init(require 'config/application.config.php');

use \Celaeno\ORM\Types\DateTimeMsType;
use \Eve\Model;
use \Eve\Model\Repositories;

/** @var $em \Doctrine\ORM\EntityManager */
$em = \Eve\Module::getEntityManager();

function listAll(\Doctrine\ORM\EntityManager $em, $model_name)
{
    $repo = $em->getRepository($model_name);

    $entities = $repo->findBy(array(), array('name' => 'ASC'), 10);

    foreach ($entities as $entity)
    {
        echo $entity . PHP_EOL;
    }
}

// Actual testing

try
{
    listAll($em, '\Eve\Model\Item');
}
catch (Exception $e)
{
    echo $e->getMessage() . PHP_EOL;
}

echo "Done." . PHP_EOL;

?>
