<?php

class Constellations extends BaseMigrator
{
    const TABLE_NAME = 'constellations';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                c.constellationID AS original_id,
                c.constellationName,
                c.x,
                c.y,
                c.z,
                c.xMin,
                c.xMax,
                c.yMin,
                c.yMax,
                c.zMin,
                c.zMax,
                c.radius,
                f.itemName AS faction_name,
                r.itemName AS region_name
            FROM mapConstellations AS c
                LEFT JOIN invNames AS r ON (r.itemID = c.regionID)
                LEFT JOIN invNames AS f ON (f.itemID = c.factionID)
        ');

        $this->beginCopyTo('constellations');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['constellationName'],
                $row['original_id'],
                $row['region_name'],
                $row['faction_name'],
                $row['x'],
                $row['y'],
                $row['z'],
                $row['xMin'],
                $row['xMax'],
                $row['yMin'],
                $row['yMax'],
                $row['zMin'],
                $row['zMax'],
                $row['radius'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
