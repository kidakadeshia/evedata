<?php

class Factions extends BaseMigrator
{
    const TABLE_NAME = 'factions';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                f.factionName AS faction_name,
                f.factionID AS original_id,
                f.description,
                f.sizeFactor,
                s.itemName AS planetary_system_name,
                c1.itemName AS corporation_name,
                c2.itemName AS militia_corporation_name,
                r.raceName AS race_name
            FROM chrFactions AS f
                LEFT JOIN invNames AS s ON (s.itemID = f.solarSystemID)
                LEFT JOIN invNames AS c1 ON (c1.itemID = f.corporationID)
                LEFT JOIN invNames AS c2 ON (c2.itemID = f.militiaCorporationID)
                LEFT JOIN chrRaces AS r ON (r.raceID = f.raceIDs)
        ');

        $this->beginCopyTo('factions');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['faction_name'],
                $row['original_id'],
                $row['planetary_system_name'],
                $row['corporation_name'],
                $row['militia_corporation_name'],
                $row['race_name'],
                $row['description'],
                $row['sizeFactor'],
                $this->pgNow(),
                null
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
