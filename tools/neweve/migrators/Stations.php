<?php

class Stations extends BaseMigrator
{
    const TABLE_NAME = 'stations';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                s.stationID AS original_id,
                s.stationName AS name,
                s.security,
                s.dockingCostPerVolume AS docking_cost_per_volume,
                s.maxShipVolumeDockable AS max_ship_volume_dockable,
                s.officeRentalCost AS office_rental_cost,
                s.x,
                s.y,
                s.z,
                s.reprocessingEfficiency AS reprocessing_efficiency,
                s.reprocessingStationsTake AS reprocessing_stations_take,
                s.reprocessingHangarFlag AS reprocessing_hangar_flag,
                o.operationName AS operation_name,
                pop.itemName AS planet_name,
                pom.itemName AS moon_name,
                c.itemName AS corporation_name,
                st.itemName AS station_type_name
            FROM staStations AS s
                LEFT JOIN staOperations AS o ON (o.operationID = s.operationID)
                LEFT JOIN mapDenormalize AS dn ON (dn.itemID = s.stationID)
                LEFT JOIN mapDenormalize AS pom ON (pom.itemID = dn.orbitID AND pom.groupID = 8)
                LEFT JOIN mapDenormalize AS pop ON (pop.itemID = dn.orbitID AND pop.groupID = 7)
                LEFT JOIN invNames AS c ON (c.itemID = s.corporationID)
                LEFT JOIN invNames AS st ON (st.itemID = s.stationTypeID)
        ');

        $this->beginCopyTo('stations');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['name'],
                $row['original_id'],
                $row['operation_name'],
                $row['planet_name'],
                $row['moon_name'],
                $row['corporation_name'],
                $row['security'],
                $row['docking_cost_per_volume'],
                $row['max_ship_volume_dockable'],
                $row['office_rental_cost'],
                $row['x'],
                $row['y'],
                $row['z'],
                $row['reprocessing_efficiency'],
                $row['reprocessing_stations_take'],
                $row['reprocessing_hangar_flag'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
