<?php

abstract class BaseMigrator
{
    const CONN_EVEDBO = 'evedbo';
    const CONN_NEWEVE = 'neweve';

    const PG_COPY_ROWS_INTERVAL = 10000;

    private $neweve_conn;

    private $evedbo_conn;

    private $affected_rows;

    private $copying_to;

    private $copying_rows;

    private $copying_count;

    private $now;

    public function __construct($now, $neweve_conn, $evedbo_conn)
    {
        $this->now = $now;
        $this->affected_rows = 0;
        $this->neweve_conn = $neweve_conn;
        $this->evedbo_conn = $evedbo_conn;
        $this->copying_to = null;
        $this->copying_rows = null;
        $this->copying_count = 0;
    }

    public function setup()
    {
    }

    public function processMigration()
    {
    }

    public function teardown()
    {
    }

    public function getAffectedRows()
    {
        return $this->affected_rows;
    }

    protected function updateAffectedRows($result)
    {
        $this->affected_rows += pg_affected_rows($result);

        if ($this->affected_rows != 0 && ($this->affected_rows % 500) == 0)
        {
            echo get_class($this) . " has affected " . $this->affected_rows . " row(s)...\n";
        }
    }

    protected function getConnection($conn)
    {
        if ($conn === self::CONN_EVEDBO)
        {
            return $this->evedbo_conn;
        }
        else if ($conn === self::CONN_NEWEVE)
        {
            return $this->neweve_conn;
        }

        throw new Exception("Attempt to get a unknown connection");
    }

    protected function beginCopyTo($table_name)
    {
        echo "Beginning copy to " . $table_name . "." . PHP_EOL;

        $this->copying_to = $table_name;
        $this->copying_rows = array();
    }

    protected function copyRow($row)
    {
        $actual_row_data = '';

        for ($i = 0; $i < count($row); $i++)
        {
            $scalar = preg_replace('/\r|\n|\r\n/', '\n', $row[$i]);

            if (is_null($scalar))
            {
                throw new Exception("Failed to exec preg_replace on " . var_export($scalar));
            }

            if ($scalar === null || (is_string($scalar) && strlen($scalar) == 0))
            {
                $actual_row_data .= "\\N";
            }
            else
            {
                $actual_row_data .= $scalar;
            }

            if ($i != (count($row) - 1))
                $actual_row_data .= "\t";
        }

        $this->copying_rows[] = $actual_row_data;

        if ((count($this->copying_rows) % self::PG_COPY_ROWS_INTERVAL) == 0)
            $this->copyActualRows();
    }

    protected function copyActualRows()
    {
        $ret = pg_copy_from($this->getConnection(self::CONN_NEWEVE), $this->copying_to, $this->copying_rows);

        $this->copying_count += count($this->copying_rows);

        $mem_usage = number_format(memory_get_usage() / 1024 / 1024, 4);

        echo "[" . $mem_usage . " MB]: Copied " . $this->copying_count . " row(s) to " . $this->copying_to . PHP_EOL;

        if (!$ret)
            throw new Exception("pg_copy_from failed");

        $this->copying_rows = array();
    }

    protected function endCopyTo()
    {
        if (!empty($this->copying_rows))
        {
            $this->copyActualRows();
        }

        echo "Ended copying to " . $this->copying_to . "." . PHP_EOL;

        $this->copying_to = null;
        $this->copying_rows = null;
    }

    protected function pgNow()
    {
        $time = gettimeofday();
        $str = date('Y-m-d H:i:s', $time['sec']);
        $str .= '.' . $time['usec'];
        return $this->now;
    }

    protected function pgPrepare($conn, $stmtname, $query)
    {
        $ret = pg_prepare($this->getConnection($conn), $stmtname, $query);

        if ($ret === false)
            throw new Exception("pg_prepare failed");

        return $ret;
    }

    protected function pgExecute($conn, $stmtname, $params)
    {
        $ret = pg_execute($this->getConnection($conn), $stmtname, $params);

        if ($ret === false)
            throw new Exception("pg_execute failed");

        $this->updateAffectedRows($ret);

        return $ret;
    }

    protected function pgQuery($conn, $query)
    {
        $ret = pg_query($this->getConnection($conn), $query);

        if ($ret === false)
            throw new Exception("pg_query failed");

        return $ret;
    }

    protected function pdoQuery($conn, $query)
    {
        $ret = $this->getConnection($conn)->query($query);

        if ($ret === false)
        {
            $info = $this->getConnection($conn)->errorInfo();

            throw new Exception("PDO::Query failed: " . $info[2]);
        }

        return $ret;
    }

    protected function pdoPrepare($conn, $query)
    {
        $stmt = $this->getConnection($conn)->prepare($query);

        if ($stmt === false)
        {
            $info = $this->getConnection($conn)->errorInfo();

            throw new Exception("PDO::Prepare failed: " . $info[2]);
        }

        return $stmt;
    }

    protected function pgFetchArray($result)
    {
        return pg_fetch_array($result, null, PGSQL_ASSOC);
    }
}
