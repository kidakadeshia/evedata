<?php

class SkillAttributes extends BaseMigrator
{
    const TABLE_NAME = 'skill_attributes';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                t.typeName AS item_name,
                at.attributeName AS attribute_name,
                ts.typeName AS skill_name
            FROM dgmTypeAttributes AS a
                INNER JOIN dgmAttributeTypes AS at ON (at.attributeID = a.attributeID AND at.attributeName IN (\'requiredSkill1\',
                                                                                                               \'requiredSkill2\',
                                                                                                               \'requiredSkill3\',
                                                                                                               \'requiredSkill4\',
                                                                                                               \'requiredSkill5\',
                                                                                                               \'requiredSkill6\')
                                                      )
                INNER JOIN invTypes AS t ON (t.typeID = a.typeID)
                INNER JOIN invTypes AS ts ON (ts.typeID = a.valueInt)
            WHERE t.marketGroupID IS NOT NULL AND ts.marketGroupID IS NOT NULL
        ');

        /** @var PDOStatement $stmt */
        $stmt = $this->pdoPrepare(self::CONN_EVEDBO, '
            SELECT
                a.valueFloat AS level1,
                a.valueInt AS level2
            FROM dgmTypeAttributes AS a
                INNER JOIN dgmAttributeTypes AS at ON (at.attributeID = a.attributeID AND at.attributeName IN (\'requiredSkill1Level\',
                                                                                                               \'requiredSkill2Level\',
                                                                                                               \'requiredSkill3Level\',
                                                                                                               \'requiredSkill4Level\',
                                                                                                               \'requiredSkill5Level\',
                                                                                                               \'requiredSkill6Level\')
                                                      )
                INNER JOIN invTypes AS t ON (t.typeID = a.typeID)
            WHERE t.marketGroupID IS NOT NULL AND t.typeName = :item_name AND at.attributeName = :attribute_name
        ');

        $this->beginCopyTo('skill_attributes');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $stmt->execute(array(
                'item_name' => $row['item_name'],
                ':attribute_name' => $row['attribute_name'] . 'Level'
            ));

            $skill_levels = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($skill_levels === false)
            {
                continue;
            }

            $level = isset($skill_levels['level1']) ? $skill_levels['level1'] : $skill_levels['level2'];

            if (is_null($level))
            {
                break;
            }

            $copy_row = array(
                $row['attribute_name'],
                $row['item_name'],
                $row['skill_name'],
                (int) $level,
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
