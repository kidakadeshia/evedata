<?php

class ShipTypes extends BaseMigrator
{
    const TABLE_NAME = 'ship_types';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                g.groupID AS original_id,
                g.groupName AS ship_types_name,
                g.description,
                g.useBasePrice AS use_base_price,
                g.published
            FROM invGroups AS g WHERE g.categoryID = 6
        ');

        $this->beginCopyTo('ship_types');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['ship_types_name'],
                $row['original_id'],
                $row['description'],
                $row['use_base_price'],
                $row['published'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
