<?php

class Planets extends BaseMigrator
{
    const TABLE_NAME = 'planets';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                n.itemName AS name,
                n.itemID AS original_id,
                pt.typeName AS planet_type_name,
                dn.x,
                dn.y,
                dn.z,
                cs.temperature,
                cs.radius,
                cs.orbitRadius AS orbit_radius,
                cs.orbitPeriod AS orbit_period,
                cs.eccentricity,
                cs.massDust AS mass_dust,
                cs.massGas AS mass_gas,
                cs.density,
                cs.surfaceGravity AS surface_gravity,
                cs.escapeVelocity AS escape_velocity,
                cs.rotationRate AS rotation_rate,
                cs.locked,
                cs.pressure,
                dn.celestialIndex AS orbit_position,
                dns.itemName AS star_name
            FROM mapDenormalize AS dn
                INNER JOIN invNames AS n ON (n.itemID = dn.itemID)
                INNER JOIN mapDenormalize dns ON (dns.itemID = dn.orbitID)
                INNER JOIN invTypes AS pt ON (pt.typeID = dn.typeID)
                INNER JOIN mapCelestialStatistics AS cs ON (cs.celestialID = dn.itemID)
            WHERE dn.groupID = 7
        ');

        $this->beginCopyTo('planets');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['name'],
                $row['original_id'],
                $row['star_name'],
                $row['planet_type_name'],
                $row['x'],
                $row['y'],
                $row['z'],
                $row['radius'],
                $row['temperature'],
                $row['orbit_radius'],
                $row['orbit_period'],
                $row['eccentricity'],
                $row['mass_dust'],
                $row['mass_gas'],
                $row['density'],
                $row['surface_gravity'],
                $row['escape_velocity'],
                $row['rotation_rate'],
                $row['locked'],
                $row['pressure'],
                $row['orbit_position'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
