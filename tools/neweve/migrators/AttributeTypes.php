<?php

class AttributeTypes extends BaseMigrator
{
    const TABLE_NAME = 'attribute_types';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                at.attributeName AS attribute_name,
                at.attributeID AS original_id,
                at.description,
                at.published,
                at.displayName AS display_name,
                at.stackable,
                at.highIsGood AS high_is_good,
                c.categoryName AS category_name
            FROM dgmAttributeTypes AS at
                LEFT JOIN invCategories AS c ON (c.categoryID = at.categoryID);
        ');

        $this->beginCopyTo('attribute_types');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['attribute_name'],
                $row['original_id'],
                $row['category_name'],
                $row['description'],
                $row['published'],
                $row['display_name'],
                $row['stackable'],
                $row['high_is_good'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
