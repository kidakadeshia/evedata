<?php

class TemplateMigrator extends BaseMigrator
{
    const TABLE_NAME = 'template_migrator';

    public function processMigration()
    {
        $result = $this->pgQuery(self::CONN_EVEDBO,
            'SELECT
             FROM template_migrator AS ss
             LEFT JOIN invnames AS c ON (c.itemid = ss.constellationid)'
        );

        $this->beginCopyTo('template_migrator');

        while (($row = $this->pgFetchArray($result)))
        {
            $copy_row = array(
                $row['template_migratorname'],
                $row['itemid'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
