<?php

class Corporations extends BaseMigrator
{
    const TABLE_NAME = 'corporations';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
             SELECT
                 cn.itemName AS corporationname,
                 c.corporationID AS original_id,
                 c.size,
                 c.extent,
                 c.publicShares,
                 c.initialPrice,
                 c.minSecurity,
                 c.scattered,
                 c.sizeFactor,
                 c.description,
                 s.itemName AS planetary_system_name,
                 f.itemName AS faction_name,
                 cf.itemName AS friend_corporation_name,
                 ce.itemName AS enemy_corporation_name
             FROM crpNPCCorporations AS c
                 LEFT JOIN invNames AS s ON (s.itemID = c.solarSystemID)
                 LEFT JOIN invNames AS f ON (f.itemID = c.factionID)
                 LEFT JOIN invNames AS cn ON (cn.itemID = c.corporationID)
                 LEFT JOIN invNames AS cf ON (cf.itemID = c.friendID)
                 LEFT JOIN invNames AS ce ON (ce.itemID = c.enemyID)
        ');

        $this->beginCopyTo(self::TABLE_NAME);

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['corporationname'],
                $row['original_id'],
                $row['planetary_system_name'],
                $row['faction_name'],
                $row['friend_corporation_name'],
                $row['enemy_corporation_name'],
                $row['size'],
                $row['extent'],
                $row['publicShares'],
                $row['initialPrice'],
                $row['minSecurity'],
                $row['scattered'],
                $row['sizeFactor'],
                $row['description'],
                $this->pgNow(),
                null
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
