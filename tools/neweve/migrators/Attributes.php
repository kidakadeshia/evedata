<?php

class Attributes extends BaseMigrator
{
    const TABLE_NAME = 'attributes';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                t.typeName AS item_name,
            at.attributeName AS attribute_type_name
            FROM dgmTypeAttributes AS a
                INNER JOIN invTypes AS t ON (t.typeID = a.typeID AND t.marketGroupID IS NOT NULL)
                INNER JOIN dgmAttributeTypes AS at ON (at.attributeID = a.attributeID)
        ');

        $this->beginCopyTo('attributes');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['attribute_type_name'],
                $row['item_name'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
