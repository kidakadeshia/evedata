<?php

class PlanetarySystemJumps extends BaseMigrator
{
    const TABLE_NAME = 'planetary_system_jumps';

    public function processMigration()
    {
        $result = $this->pdoQuery(self::CONN_EVEDBO, '
            SELECT
                fss.itemName AS from_solar_system_name,
                tss.itemName AS to_solar_system_name
            FROM mapSolarSystemJumps AS ssj
                LEFT JOIN invNames AS fss ON (fss.itemID = ssj.fromSolarSystemID)
                LEFT JOIN invNames AS tss ON (tss.itemID = ssj.toSolarSystemID)
        ');

        $this->beginCopyTo('planetary_system_jumps');

        while (($row = $result->fetch(PDO::FETCH_ASSOC)))
        {
            $copy_row = array(
                $row['from_solar_system_name'],
                $row['to_solar_system_name'],
                $this->pgNow(),
                null,
            );

            $this->copyRow($copy_row);
        }

        $this->endCopyTo();
    }
}
