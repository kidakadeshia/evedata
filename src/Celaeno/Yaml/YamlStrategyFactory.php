<?php

namespace Celaeno\Yaml;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class YamlStrategyFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $viewRenderer = $serviceLocator->get('YamlRenderer');
        return new YamlStrategy($viewRenderer);
    }
}
