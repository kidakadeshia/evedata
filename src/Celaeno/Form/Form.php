<?php

namespace Celaeno\Form;

class Form
{
    protected $name;
    protected $values = array();
    protected $widgets = array();

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function render()
    {
        $renderer = Renderer::getInstance('/view/eve/test/form');

        $variables = array(
            'form' => $this
        );

        return $renderer->render($variables, 'form');
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue($name)
    {
        return $this->values[$name];
    }

    public function setValue($name)
    {
        $this->values[$name] = $name;
    }

    public function addWidget(Widget $widget)
    {
        $this->widgets[] = $widget;

        $widget->setForm($this);
    }

    public function removeWidget(Widget $widget)
    {
        $idx = array_search($widget, $this->widgets);

        unset($this->widgets[$idx]);
    }

    public function getWidget($name)
    {
        foreach ($this->widgets as $widget)
        {
            if ($widget->getName() == $name)
            {
                return $this->widget;
            }
        }

        return null;
    }

    /** @returns Widget[] */
    public function getWidgets()
    {
        return $this->widgets;
    }
}
