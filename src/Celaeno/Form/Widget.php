<?php

namespace Celaeno\Form;

use Eve\Module;


abstract class Widget
{
    protected $template;
    protected $name;
    protected $default = null;
    protected $label = 'No Label';
    protected $form = null;

    public function __construct($template, $name)
    {
        $this->template = $template;
        $this->name = $name;
    }

    public function render()
    {
        $renderer = Renderer::getInstance('/view/eve/test/widget');

        $variables = array(
            'name' =>     $this->name,
            'formName' => (!is_null($this->form) ? $this->form->getName() : null),
            'value' =>    $this->getValue(),
            'default' =>  $this->default,
        );

        return $renderer->render($variables, $this->template);
    }

    public function getForm()
    {
        return $this->form;
    }

    public function setForm(Form $form)
    {
        $this->form = $form;
    }

    public function getDefault()
    {
        return $this->default;
    }

    protected function setDefault($default)
    {
        $this->default = $default;
    }

    public function getName()
    {
        return $this->name;
    }

    protected function setName($name)
    {
        $this->name = $name;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function getLabel()
    {
        return $this->label;
    }

    protected function setLabel($label)
    {
        $this->label = $label;
    }

    public function getValue()
    {
        if (!is_null($this->form))
        {
            return $this->form->getValue($this->name);
        }

        return null;
    }

    public function __toString()
    {
        return $this->name;
    }
}