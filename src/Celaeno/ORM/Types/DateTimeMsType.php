<?php

namespace Celaeno\ORM\Types;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class DateTimeMsType extends Type
{
    const SQL_TYPE = 'TIMESTAMP';

    const NAME = 'DateTimeMs';

    const DATETIME_FORMAT = 'Y-m-d H:i:s.u';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return self::SQL_TYPE;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null)
        {
            return null;
        }

        return \DateTime::createFromFormat(self::DATETIME_FORMAT, $value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null)
        {
            return null;
        }

        return $value->format(self::DATETIME_FORMAT);
    }

    public function getName()
    {
        return self::NAME;
    }

    public static function registerType(Connection $conn)
    {
        Type::addType(self::NAME, '\Celaeno\ORM\Types\DateTimeMsType');
        $conn->getDatabasePlatform()->registerDoctrineTypeMapping(self::NAME, self::NAME);
    }
}