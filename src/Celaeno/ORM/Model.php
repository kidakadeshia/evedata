<?php

namespace Celaeno\ORM;

abstract class Model implements \Serializable
{
    public abstract function getId();

    public abstract function getName();

    public abstract function getOriginalId();

    public abstract function getUpdatedOn();

    public abstract function getCreatedOn();

    public function __toString()
    {
        return '<' . $this->getId() . ': ' . $this->getName() . '>';
    }

    protected function getSerializedMapping()
    {
        return array();
    }

    public function serialize($root = true)
    {
        $result = array();

        $serializedMapping = $this->getSerializedMapping();

        $result['class'] = !isset($serializedMapping['class']) ? get_class($this) : $serializedMapping['class'];
        $result['id'] = $this->getId();
        $result['original_id'] = $this->getOriginalId();
        $result['updated_on'] = $this->getUpdatedOn();
        $result['created_on'] = $this->getCreatedOn();

        if (!$root)
        {
            return $result;
        }

        foreach ($serializedMapping['fields'] as $field)
        {
            $method = null;

            if (!isset($field['methodName']))
            {
                $method = 'get' . \Celaeno\Util::methodNameToCamelCase($field['fieldName']);
            }
            else
            {
                $method = $field['methodName'];
            }

            $ret = $this->$method();

            if (is_object($ret) && $ret instanceof Model)
            {
                $result[$field['fieldName']] = $ret->serialize(!$root);
            }
            else if (is_array($ret) || $ret instanceof \ArrayAccess)
            {
                $ret_result = array();

                // TODO: Do this properly!
                foreach ($ret as $obj)
                {
                    if (is_object($obj) && $obj instanceof Model)
                    {
                        $ret_result[] = $obj->serialize(!$root);
                    }
                }

                $result[$field['fieldName']] = $ret_result;
            }
            else if (is_scalar($ret))
            {
                $result[$field['fieldName']] = $ret;
            }
        }

        return $result;
    }

    public function unserialize($serialized)
    {
    }
}
