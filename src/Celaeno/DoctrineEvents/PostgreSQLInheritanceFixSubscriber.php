<?php

namespace Celaeno\DoctrineEvents;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;

class PostgreSQLInheritanceFixSubscriber implements EventSubscriber
{
    public function loadClassMetaData(LoadClassMetadataEventArgs $eventArgs)
    {
        $class_meta_data = $eventArgs->getClassMetadata();

        if ($class_meta_data->rootEntityName == $class_meta_data->name || empty($class_meta_data->parentClasses))
        {
            return;
        }

        foreach ($class_meta_data->fieldMappings as &$field_mapping)
        {
            if (isset($field_mapping['inherited']) && isset($field_mapping['declared']))
            {
                $field_mapping['inherited'] = $class_meta_data->name;
            }
        }
    }

    public function getSubscribedEvents()
    {
        return array(Events::loadClassMetadata);
    }
}
