<?php

namespace Celaeno\JSON;

use Zend\Json\Json;
use Zend\Stdlib\ArrayUtils;
use Zend\View\Exception;
use Zend\View\Renderer\RendererInterface as Renderer;
use Zend\View\Resolver\ResolverInterface as Resolver;

class JsonRenderer implements Renderer
{
    protected $resolver;

    public function getEngine()
    {
        return $this;
    }

    public function setResolver(Resolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function render($nameOrModel, $values = null)
    {
        if ($nameOrModel instanceof JsonModel)
        {
            return $nameOrModel->serialize();
        }
    }
}
